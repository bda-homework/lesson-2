// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String t = "             1,e 6,e 0,e 9,e           ";
        String numberOnlyStr = t.replaceAll("[^0-9]", "");
        int numbersOnly = Integer.parseInt(numberOnlyStr);
        for (int i = numbersOnly; i > 0; i /= 10) {
            int everyNumber = i % 10;
            int squareNumber = (int) Math.pow(everyNumber, 2);
            System.out.println(everyNumber + " reqemin kavdrati = " + squareNumber);
        }

    }
}